//
// Created by Rostislav on 09.01.2022.
//

#ifndef ASSIGNMENT_MEMORY_ALLOCATOR_TEST_H
#define ASSIGNMENT_MEMORY_ALLOCATOR_TEST_H

#include "mem.h"
#include "mem_internals.h"

bool test_heap_init(void *heap);
bool test_free_block(void *heap);
bool test_free_blocks(void *heap);
bool test_extends_region(void *heap);
bool test_new_region(void *heap);

#endif //ASSIGNMENT_MEMORY_ALLOCATOR_TEST_H
