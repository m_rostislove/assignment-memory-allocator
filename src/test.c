//
// Created by Rostislav on 09.01.2022.
//

#include "test.h"

#define BLOCK_MIN_CAPACITY 24

static struct block_header* block_get_header(void* contents) {
    return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

bool test_heap_init(void *heap){
    debug_heap(stderr, heap);
    void* block1 = _malloc(0);
    void* block2 = _malloc(100);
    struct block_header* header1 = block_get_header(block1);
    struct block_header* header2 = block_get_header(block2);
    if(header1->capacity.bytes != BLOCK_MIN_CAPACITY || header2->capacity.bytes != 100) {
        fprintf(stderr, "Error at allocating blocks\n");
        return false;
    }
    debug_heap(stdout, heap);
    if(!block1)
        return false;
    _free(block1);
    _free(block2);
    return true;
}

bool test_free_block(void *heap){
    debug_heap(stderr, heap);
    void* block1 = _malloc(0);
    void* block2 = _malloc(100);
    struct block_header* header1 = block_get_header(block1);
    struct block_header* header2 = block_get_header(block2);
    if(header1->capacity.bytes != BLOCK_MIN_CAPACITY || header2->capacity.bytes != 100) {
        fprintf(stderr, "Error at allocating blocks\n");
        return false;
    }
    debug_heap(stderr, heap);
    _free(block1);
    debug_heap(stderr, heap);
    if(!header1->is_free) {
        fprintf(stderr, "Error at freeing one block\n");
        return false;
    }
    _free(block2);
    return true;
}

bool test_free_blocks(void *heap){
    debug_heap(stderr, heap);
    void* block1 = _malloc(0);
    void* block2 = _malloc(100);
    void* block3 = _malloc(1000);
    struct block_header* header1 = block_get_header(block1);
    struct block_header* header2 = block_get_header(block2);
    struct block_header* header3 = block_get_header(block3);
    if(header1->capacity.bytes != BLOCK_MIN_CAPACITY || header2->capacity.bytes != 100 || header3->capacity.bytes != 1000) {
        fprintf(stderr, "Error at allocating blocks\n");
        return false;
    }
    debug_heap(stderr, heap);
    _free(block1);
    _free(block2);
    debug_heap(stderr, heap);
    if(!header1->is_free || !header2->is_free){
        fprintf(stderr, "Error at freeing two blocks\n");
        return false;
    }
    _free(block3);
    return true;
}

bool test_extends_region(void *heap){
    debug_heap(stderr, heap);
    void* block1 = _malloc(10000);
    void* block2 = _malloc(3000);
    struct block_header* header1 = block_get_header(block1);
    struct block_header* header2 = block_get_header(block2);
    if(header1->capacity.bytes != 10000 || header2->capacity.bytes != 3000) {
        fprintf(stderr, "Error at allocating blocks\n");
        return false;
    }
    debug_heap(stderr, heap);
    if(header1->next != header2) {
        fprintf(stderr, "Error at extending existing memory region\n");
        return false;
    }
    _free(block1);
    _free(block2);
    return true;
}
bool test_new_region(void *heap){
    debug_heap(stderr, heap);
    void* block1 = _malloc(9500);

    struct block_header *header1 = block_get_header(block1);
    if(header1->capacity.bytes != 9500) {
        fprintf(stderr, "Error at allocating first block\n");
        return false;
    }
    while (header1->next) header1 = header1->next;

    map_pages(header1->contents + header1->capacity.bytes, 3000, MAP_FIXED);

    debug_heap(stderr, heap);

    void* block2 = _malloc(20000);
    struct block_header *header2 = block_get_header(block2);

    debug_heap(stderr, heap);

    if(header2->is_free || header2->capacity.bytes != 20000) {
        fprintf(stderr, "Error allocating second block in new region\n");
        return false;
    }
    _free(block1);
    _free(block2);
    return true;
}
