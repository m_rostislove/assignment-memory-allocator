//
// Created by Rostislav on 10.01.2022.
//

#include "test.h"
#include "util.h"

int main(){
    void *heap = heap_init(10000);
    if(!test_heap_init(heap))
        err("Failed at normal memory allocation\n");
    else printf("Normal successful memory allocation\n");
    if(!test_free_block(heap))
        err("Failed at freeing one block from several allocated ones\n");
    else printf("Successful freeing one block from several allocated ones\n");
    if(!test_free_blocks(heap))
        err("Failed at freeing two blocks from several allocated ones\n");
    else printf("Successful freeing two blocks from several allocated ones\n");
    if(!test_extends_region(heap))
        err("Failed at expanding the old old memory region with new one\n");
    else printf("Successful expanding the old memory region with new one\n");
    if(!test_new_region(heap))
        err("Failed at allocating new separated memory region\n");
    else printf("Successful allocating new separated memory region\n");

    return 0;

}